<%@page import="com.db.fm.DealLoader"%>
<%@page import="com.db.fm.Deal"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="User" class="com.db.fm.User" scope="session"/>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>Trade Flow Monitor</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <% ArrayList<Deal> deals = DealLoader.loadAllDeals(); %>
        <table>
              <tr>
                <th>ID</th>
                <th>Time</th>
                <th>Counterparty</th>
                <th>Instrument</th>
                <th>Type</th>
                <th>Amount</th>
                <th>Quantity</th>
            </tr>
        <% for (Deal deal: deals) { %>
        <tr>
            <td><%=deal.getId()%></td>
            <td><%=deal.getTime()%></td>
            <td><%=deal.getCounterparty()%></td>
            <td><%=deal.getInstrument()%></td>
            <td><%=deal.getType()%></td>
            <td><%=deal.getAmount()%></td>
            <td><%=deal.getQuantity()%></td>
        </tr>
        <%
        }  
        System.out.println("Finished printing deals");
        %>
        
        </table>
    </body>
</html>
