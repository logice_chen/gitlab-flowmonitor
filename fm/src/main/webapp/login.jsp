<%@page import="com.db.fm.UserCheck"%>
<%@page import="com.db.fm.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="User" class="com.db.fm.User" scope="session"/>

<jsp:setProperty property="*" name="User"/>

<%
    String userID = request.getParameter("username");
    String userPassword = request.getParameter("password");
    User myUser = new User();
    myUser.setUserID(userID);
    myUser.setUserPwd(userPassword);
    boolean status = UserCheck.validate(myUser);
    if(status){
        System.out.println("You r successfully logged in");
        session.setAttribute("session","TRUE");
    %>
    <h2>You have successfully logged in!</h2>  
<jsp:include page="dashboard.jsp"></jsp:include>    
    <%
    }
    else
    {
        request.setAttribute("errorMessage", "Invalid user or password");
        request.getRequestDispatcher("index.jsp").forward(request, response);
        System.out.println("Sorry, email or password error");
%>
<jsp:include page="index.jsp"/>
<%
    }
%>
