<%@ page import="com.db.fm.DBEngine" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="theDBEngine" class="com.db.fm.DBEngine" scope="application"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Flowmonitor Login</title>
    </head>
    <!-- Error message after forward from login.jsp -->
    <% if (request.getAttribute("errorMessage") != null)
    {
        out.println(request.getAttribute("errorMessage"));
    }
    %>
    <body>

        <h1>Hi, FlowMonitor user!</h1>

        <ul>
            <li><a href="">Home</a></li>
            <li><a href="login">Login</a></li>
        </ul>

        <%
            String databaseStatus = "Connection is lost";
            DBEngine myConnection = new DBEngine();
            if (myConnection.getStatus())
            {
                databaseStatus = "DataBase is on";
            }
        %>
        <h2> <%= databaseStatus %></h2>

        <%
            if (myConnection.getStatus())
            {
        %>
        <form action="login.jsp" method="GET">
            <h3>Username:</h3>
            <input type="text" name="username">

            <h3>Password:</h3>
            <input type="text" name="password">

            <input type = "submit" value = "Log in" />
            <%
                }
            %>
        </form>
        
    </body>
</html>
