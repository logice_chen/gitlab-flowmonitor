package com.db.fm;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

public class ReadProperties {
    private Properties itsProperties = new Properties();

    private String defaultNamePropertiesFile = "databaseConnector.properties";

    public static void main(String[] args) {
        new ReadProperties();
    }

    public ReadProperties() {
        readPropertiesFromFile(defaultNamePropertiesFile);
    }
    
    public ReadProperties(String namePropertiesFile) {
        readPropertiesFromFile(namePropertiesFile);
    }
    
    private void readPropertiesFromFile(String namePropertiesFile) {
        InputStream inputStream = null;
        try
        {
            inputStream = getClass().getClassLoader().getResourceAsStream(namePropertiesFile);
            
            if (inputStream != null) 
            {
                    itsProperties.load(inputStream);
                    inputStream.close();
            } 
            else 
            {
                inputStream.close();
                throw new FileNotFoundException("Property file '" + namePropertiesFile + "' was not found");
            }
            inputStream.close();
        }
        catch (Exception e) {}
    }
    
    public Properties getProperties()
    {
        return itsProperties;
    }
}
