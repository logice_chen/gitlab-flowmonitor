package com.db.fm;

import java.sql.*;

public class UserCheck {

    public static boolean validate(User bean){
        boolean status = false;

        try {
            DBEngine dbEngine = new DBEngine();
            Connection con = dbEngine.getConnector();

            PreparedStatement ps = con.prepareStatement(
                    "select * from db_grad_cs_1917.users where user_id=? and user_pwd=?");

            ps.setString(1, bean.getUserID());
            ps.setString(2, bean.getUserPwd());

            ResultSet rs=ps.executeQuery();
            status=rs.next();
            rs.close();
            dbEngine.close();
        }
        catch(Exception e){}

        return status;
    }
}
