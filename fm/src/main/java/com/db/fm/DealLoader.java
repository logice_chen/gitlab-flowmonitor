package com.db.fm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.ArrayList;

public class DealLoader {
      public static ArrayList<Deal> loadAllDeals(){
        ArrayList deals = new ArrayList<Deal>();

        try
		{
            
            // create Database connection
            DBEngine dbEngine = new DBEngine();
            Connection con = dbEngine.getConnector();

            PreparedStatement ps = con.prepareStatement(
                    "select deal_id, deal_time, deal_counterparty_id, "
                            + "deal_instrument_id, deal_type,deal_amount, "
                            + "deal_quantity from db_grad_cs_1917.deal");
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()) {
                Deal deal = new Deal();
                deal.setId(rs.getInt("deal_id"));
                deal.setTime(rs.getTimestamp("deal_time"));
                deal.setCounterparty(rs.getInt("deal_counterparty_id"));
                deal.setInstrument(rs.getInt("deal_instrument_id"));
                deal.setType(rs.getString("deal_type"));
                deal.setAmount(rs.getDouble("deal_amount"));
                deal.setQuantity(rs.getInt("deal_quantity"));
                deals.add(deal);
            }
            System.out.println("Successfully retrieved deals");
            rs.close();
            dbEngine.close();
        }
        catch(Exception e)
        {
            System.out.println("Could not retrieve deals: "+e);
        }
        return deals;
     }
}
