package com.db.fm;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class DBEngine {
    private Connection itsConnection;
    private boolean connectionExist = false;
    
    private String databaseDriver ="";
    private String databasePath = "";
    private String databaseName = "";
    private String databaseUser = "";
    private String databasePassword = "";

    private String defaultNamePropertiesFile = "databaseConnector.properties";

    public static void main(String[] args) {
        new DBEngine();
    }

    public DBEngine()
    {
        Properties properties;
        properties = new ReadProperties(defaultNamePropertiesFile).getProperties();
        
        execute(properties);
    }
    
    public DBEngine(String pathToPropertiesFile) {
        ReadProperties properties = new ReadProperties(pathToPropertiesFile);
        execute(properties.getProperties());
    }

    private void execute(Properties properties)
    {
        try
        {
            databaseDriver = properties.getProperty("dbDriver");
            databasePath = properties.getProperty("dbPath");
            databaseName = properties.getProperty("dbName");
            databaseUser = properties.getProperty("dbUser");
            databasePassword = properties.getProperty("dbPwd");

            Class.forName(databaseDriver);
            itsConnection = DriverManager.getConnection(databasePath + databaseName, databaseUser, databasePassword);
            System.out.println(itsConnection.getCatalog());
            connectionExist = true;
        }
        catch (ClassNotFoundException | SQLException e)
        {
            e.printStackTrace();
        }
    }

    public Connection getConnector() {
        return this.itsConnection;
    }

    public void close() throws SQLException {
        try {
            itsConnection.close();
        } catch (SQLException e) {
            System.out.println("Database failed to close: "+e);
        }
    }
        
    public boolean getStatus() {
        return connectionExist;
    }
}
