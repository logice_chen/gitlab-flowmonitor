package com.db.fm;

import javax.mail.internet.*;

public class User
{
    private String  userID;
    private String  userPassword;

    public User()
    { 
    }
    
    public  User(String _userID, String _userPassword)
    {
        userID = _userID;
        userPassword = _userPassword;
    }

    public String getUserID()
    {
        return userID;
    }

    public void setUserID(String itsUserID)
    {
        this.userID = itsUserID;
    }

    public String getUserPwd()
    {
        return userPassword;
    }

    public void setUserPwd(String itsUserPwd)
    {
        this.userPassword = itsUserPwd;
    }
}
