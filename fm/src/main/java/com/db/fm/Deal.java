package com.db.fm;

import java.util.Date;

public class Deal {
    private int id;
    private Date time;
    private int counterparty;
    private int instrument;
    private String type;
    private double amount;
    private int quantity;

    public Deal() {
        
    }

    public Deal(int id, Date time, int counterparty, int instrument, String type, double amount, int quantity) {
        this.id = id;
        this.time = time;
        this.counterparty = counterparty;
        this.instrument = instrument;
        this.type = type;
        this.amount = amount;
        this.quantity = quantity;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getCounterparty() {
        return counterparty;
    }

    public void setCounterparty(int counterparty) {
        this.counterparty = counterparty;
    }

    public int getInstrument() {
        return instrument;
    }

    public void setInstrument(int instrument) {
        this.instrument = instrument;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
