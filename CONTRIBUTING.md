We have 2 non-pushed branches: master and develop.

'master' branch is our production - stable-stable version of our product, which we can deploy with docker in any time.
'develop' branch is for our current developing. Stable as well. 

To create your own branch you need to check out from 'develop' branch and specify its name with describing on what part you are working.

Some rules (just good practise in enterprise teams):
1. Begin name of your branch from 'feature/...'. It will be easier to find your own developing branch.
2. Don't use your name in the branch name. :)

Just examples:
    - feature/BE-logging
    - feature/FE-login-page-design
    - feature/SQL-find-average